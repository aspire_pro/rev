local PvPMacroModifier = CreateFrame("Frame")
local combatRequest = false
PvPMacroModifier:RegisterEvent("PLAYER_PVP_TALENT_UPDATE")
PvPMacroModifier:RegisterEvent("PLAYER_LOGIN")
PvPMacroModifier:RegisterEvent("PLAYER_REGEN_ENABLED")
PvPMacroModifier:SetScript("OnEvent", function(self, event, ...)
      if InCombatLockdown() then
         combatRequest = true
         return
      end
      if (event=="PLAYER_REGEN_ENABLED" and combatRequest) or event~="PLAYER_REGEN_ENABLED" then
         local totalPvPTalents = 0
         for k,v in pairs(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()) do 
            totalPvPTalents = totalPvPTalents+1
         end
         local totalMacros = math.ceil(totalPvPTalents/2)
         local pvpTalent = {}
         for i=1, totalPvPTalents do
            pvpTalent[i] = select(2,GetPvpTalentInfoByID(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()[i]))
         end
         --local count = 0
         --for i=1, totalMacros do
            if GetMacroInfo("PvP Talent Set 2") then
               if not pvpTalent[2] then pvpTalent[2] = "" end
               EditMacro("PvP Talent Set 2", nil, nil, "#showtooltip\n/cast [nomod]"..pvpTalent[4]..";[mod:shift,@focus]"..pvpTalent[4]..";[mod:ctrl,@mouseover]"..pvpTalent[4])
            end
            --count = count+1
         --end
         combatRequest=false
      end
end)