local PvPMacroModifier = CreateFrame("Frame")
local combatRequest = false
PvPMacroModifier:RegisterEvent("PLAYER_PVP_TALENT_UPDATE")
PvPMacroModifier:RegisterEvent("PLAYER_LOGIN")
PvPMacroModifier:RegisterEvent("PLAYER_REGEN_ENABLED")
PvPMacroModifier:SetScript("OnEvent", function(self, event, ...)
      if InCombatLockdown() then
         combatRequest = true
         return
      end
      if (event=="PLAYER_REGEN_ENABLED" and combatRequest) or event~="PLAYER_REGEN_ENABLED" then
         local totalPvPTalents = 0
         for k,v in pairs(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()) do 
            totalPvPTalents = totalPvPTalents+1
         end
         local totalMacros = math.ceil(totalPvPTalents/2)
         local pvpTalent = {}
         for i=1, totalPvPTalents do
            pvpTalent[i] = select(2,GetPvpTalentInfoByID(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()[i]))
         end
         local count = 0
         for i=1, totalMacros do
            if GetMacroInfo("PvP Talent Set "..i) then
               if not pvpTalent[i*2] then pvpTalent[i*2] = "" end
               EditMacro("PvP Talent Set "..i, nil, nil, "#showtooltip\n/cast [mod:alt] "..pvpTalent[i*2].."\n/stopmacro [mod:alt]\n/cast "..pvpTalent[i+count])
			      print (pvpTalent[1])
               if pvpTalent[1]~="Medaillon des Gladiators" then
                  print (1)
                  EditMacro("Pvp Talent Set 1", nil, nil, "#showtooltip\n/cast [mod:alt] "..pvpTalent[i*2-count-1].."\n/stopmacro [mod:alt]\n/cast Jeder für sich")
                  print (2)
               end
            end
            count = count+1
         end
         combatRequest=false
      end
end)