local PvPMacroModifier = CreateFrame("Frame")
local combatRequest = false
PvPMacroModifier:RegisterEvent("PLAYER_PVP_TALENT_UPDATE")
PvPMacroModifier:RegisterEvent("PLAYER_LOGIN")
PvPMacroModifier:RegisterEvent("PLAYER_REGEN_ENABLED")
PvPMacroModifier:SetScript("OnEvent", function(self, event, ...)
      if InCombatLockdown() then
         combatRequest = true
         return
      end
      if (event=="PLAYER_REGEN_ENABLED" and combatRequest) or event~="PLAYER_REGEN_ENABLED" then
         local totalPvPTalents = 0
         for k,v in pairs(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()) do 
            totalPvPTalents = totalPvPTalents+1
         end
         local totalMacros = math.ceil(totalPvPTalents/2)
         local pvpTalent = {}
         for i=1, totalPvPTalents do
            pvpTalent[i] = select(2,GetPvpTalentInfoByID(C_SpecializationInfo.GetAllSelectedPvpTalentIDs()[i]))
         end
            if GetMacroInfo("PvP T4") then
               if not pvpTalent[2] then pvpTalent[2] = "" end
               EditMacro("PvP T4", nil, nil, "#showtooltip\n/cast [nomod]"..pvpTalent[4]..";[mod:shift,@focus]"..pvpTalent[4]..";[mod:ctrl,@mouseover]"..pvpTalent[4])
            end
            if pvpTalent[1]=="Medaillon des Gladiators" then
               EditMacro("PvP T1", nil, nil, "#showtooltip\n/cast "..pvpTalent[1])
            else
               EditMacro("PvP T1", nil, nil, "#showtooltip\n/cast Jeder für sich")
            end
         combatRequest=false
      end
end)